'use strict';

/**
 * @ngdoc function
 * @name ssyWebApp.controller:CategoriesCtrl
 * @description
 * # CategoriesCtrl
 * Controller of the ssyWebApp
 */
angular.module('ssyWebApp')
  .controller('CategoriesCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
