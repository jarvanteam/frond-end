'use strict';

/**
 * @ngdoc function
 * @name ssyWebApp.controller:ProductsCtrl
 * @description
 * # ProductsCtrl
 * Controller of the ssyWebApp
 */
angular.module('ssyWebApp')
  .controller('ProductsCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
