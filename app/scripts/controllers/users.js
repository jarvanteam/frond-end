'use strict';

/**
 * @ngdoc function
 * @name ssyWebApp.controller:UsersCtrl
 * @description
 * # UsersCtrl
 * Controller of the ssyWebApp
 */
angular.module('ssyWebApp')
  .controller('UsersCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
