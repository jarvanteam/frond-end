'use strict';

/**
 * @ngdoc function
 * @name ssyWebApp.controller:ContactCtrl
 * @description
 * # ContactCtrl
 * Controller of the ssyWebApp
 */
angular.module('ssyWebApp')
  .controller('ContactCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
