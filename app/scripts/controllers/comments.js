'use strict';

/**
 * @ngdoc function
 * @name ssyWebApp.controller:CommentsCtrl
 * @description
 * # CommentsCtrl
 * Controller of the ssyWebApp
 */
angular.module('ssyWebApp')
  .controller('CommentsCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
