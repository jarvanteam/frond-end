'use strict';

/**
 * @ngdoc function
 * @name ssyWebApp.controller:OrdersCtrl
 * @description
 * # OrdersCtrl
 * Controller of the ssyWebApp
 */
angular.module('ssyWebApp')
  .controller('OrdersCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
