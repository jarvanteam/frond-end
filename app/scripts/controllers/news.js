'use strict';

/**
 * @ngdoc function
 * @name ssyWebApp.controller:NewsCtrl
 * @description
 * # NewsCtrl
 * Controller of the ssyWebApp
 */
angular.module('ssyWebApp')
  .controller('NewsCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
