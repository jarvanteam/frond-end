'use strict';

/**
 * @ngdoc function
 * @name ssyWebApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the ssyWebApp
 */
angular.module('ssyWebApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
