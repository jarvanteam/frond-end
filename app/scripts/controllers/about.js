'use strict';

/**
 * @ngdoc function
 * @name ssyWebApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the ssyWebApp
 */
angular.module('ssyWebApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
